# LabCOAT #



### What is LabCOAT? ###

**LabCOAT** (**Lab**oratory **C**ontracts **O**rganized **A**s **T**echnology) is a **DAO** (**D**ecentralized **A**utonomous **O**rganization) 
with **Ocean** datatokens dealing with scientific experimental data.